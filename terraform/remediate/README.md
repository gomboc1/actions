# <img src="../../assets/terraform-logo.png" style="margin-right:10px" width="24"/> Gomboc.AI Terraform Remediate Action

## When to use this action

Use this action in a deployment workflow to get remediations to your Terraform code. It can be used with either `merge_request_event` or `push` [GitLab sources](https://docs.gitlab.com/ee/ci/jobs/job_rules.html#ci_pipeline_source-predefined-variable)  

## Quickstart guide 

We recommend setting up our action on `merge_request_event`. Every time you open a PR with changes to IaC files, we will discover and scan them.

You can copy and paste these workflows from our [examples](/terraform/remediate/examples/).

## Setting up your own workflow

Your Gomboc.AI Terraform Remediate workflow should look something like this:

```
include:
  - 'https://gitlab.com/gomboc1/actions/-/raw/main/terraform/remediate/.gitlab-ci.yml'

stages:
  - example

setup-job:
  stage: example
  extends: .run-scan
  rules:
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event'
```